From 3c7423946f6d44b49994e1d78385d15633ac20e6 Mon Sep 17 00:00:00 2001
From: Brian Behlendorf <behlendorf1@llnl.gov>
Date: Wed, 30 Dec 2020 20:05:55 +0000
Subject: [PATCH] Autoconf 2.70 compatibility

Several m4 macros have been retired in autoconf 2.70.  Update the
the build system to use the new macros provided to replace them.

* Replaced AC_HELP_STRING with AS_HELP_STRING.

* Replaced AC_TRY_COMPILE with AC_COMPILE_IFELSE/AC_LANG_PROGRAM.

* Replaced AC_CANONICAL_SYSTEM with AC_CANONICAL_TARGET

* Replaced AC_PROG_LIBTOOL with LT_INIT

* $CPP is not defined in ZFS_AC_KERNEL and really shouldn't be
  directly used like this.  Replace it with an $AWK command
  to extract the kernel source version.

Signed-off-by: Brian Behlendorf <behlendorf1@llnl.gov>
---
 config/always-python.m4 |  2 +-
 config/always-pyzfs.m4  |  2 +-
 config/kernel.m4        | 14 +++++---------
 config/mount-helper.m4  |  2 +-
 config/user-dracut.m4   |  2 +-
 config/user-libexec.m4  |  2 +-
 config/user-makedev.m4  | 14 ++++++--------
 config/user-systemd.m4  | 10 +++++-----
 config/user-sysvinit.m4 |  2 +-
 config/user-udev.m4     |  4 ++--
 config/zfs-build.m4     |  2 +-
 configure.ac            |  4 ++--
 12 files changed, 27 insertions(+), 33 deletions(-)

diff --git a/config/always-python.m4 b/config/always-python.m4
index c01e631a8f4..76b06fcd848 100644
--- a/config/always-python.m4
+++ b/config/always-python.m4
@@ -7,7 +7,7 @@ dnl # set the PYTHON environment variable accordingly.
 dnl #
 AC_DEFUN([ZFS_AC_CONFIG_ALWAYS_PYTHON], [
 	AC_ARG_WITH([python],
-		AC_HELP_STRING([--with-python[=VERSION]],
+		AS_HELP_STRING([--with-python[=VERSION]],
 		[default system python version @<:@default=check@:>@]),
 		[with_python=$withval],
 		[with_python=check])
diff --git a/config/always-pyzfs.m4 b/config/always-pyzfs.m4
index f620a8f9a18..76e07b593df 100644
--- a/config/always-pyzfs.m4
+++ b/config/always-pyzfs.m4
@@ -22,7 +22,7 @@ dnl # Determines if pyzfs can be built, requires Python 2.7 or later.
 dnl #
 AC_DEFUN([ZFS_AC_CONFIG_ALWAYS_PYZFS], [
 	AC_ARG_ENABLE([pyzfs],
-		AC_HELP_STRING([--enable-pyzfs],
+		AS_HELP_STRING([--enable-pyzfs],
 		[install libzfs_core python bindings @<:@default=check@:>@]),
 		[enable_pyzfs=$enableval],
 		[enable_pyzfs=check])
diff --git a/config/kernel.m4 b/config/kernel.m4
index bd14dd4163b..f31be845f5d 100644
--- a/config/kernel.m4
+++ b/config/kernel.m4
@@ -322,19 +322,15 @@ AC_DEFUN([ZFS_AC_KERNEL], [
 	utsrelease2=$kernelbuild/include/linux/utsrelease.h
 	utsrelease3=$kernelbuild/include/generated/utsrelease.h
 	AS_IF([test -r $utsrelease1 && fgrep -q UTS_RELEASE $utsrelease1], [
-		utsrelease=linux/version.h
+		utsrelease=$utsrelease1
 	], [test -r $utsrelease2 && fgrep -q UTS_RELEASE $utsrelease2], [
-		utsrelease=linux/utsrelease.h
+		utsrelease=$utsrelease2
 	], [test -r $utsrelease3 && fgrep -q UTS_RELEASE $utsrelease3], [
-		utsrelease=generated/utsrelease.h
+		utsrelease=$utsrelease3
 	])
 
-	AS_IF([test "$utsrelease"], [
-		kernsrcver=`(echo "#include <$utsrelease>";
-		             echo "kernsrcver=UTS_RELEASE") |
-		             ${CPP} -I $kernelbuild/include - |
-		             grep "^kernsrcver=" | cut -d \" -f 2`
-
+	AS_IF([test -n "$utsrelease"], [
+		kernsrcver=$($AWK '/UTS_RELEASE/ { gsub(/"/, "", $[3]); print $[3] }' $utsrelease)
 		AS_IF([test -z "$kernsrcver"], [
 			AC_MSG_RESULT([Not found])
 			AC_MSG_ERROR([
diff --git a/config/mount-helper.m4 b/config/mount-helper.m4
index 0a6c7670840..e559b9ab273 100644
--- a/config/mount-helper.m4
+++ b/config/mount-helper.m4
@@ -1,6 +1,6 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_MOUNT_HELPER], [
 	AC_ARG_WITH(mounthelperdir,
-		AC_HELP_STRING([--with-mounthelperdir=DIR],
+		AS_HELP_STRING([--with-mounthelperdir=DIR],
 		[install mount.zfs in dir [[/sbin]]]),
 		mounthelperdir=$withval,mounthelperdir=/sbin)
 
diff --git a/config/user-dracut.m4 b/config/user-dracut.m4
index 95f800bda47..b9705297f74 100644
--- a/config/user-dracut.m4
+++ b/config/user-dracut.m4
@@ -1,7 +1,7 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_DRACUT], [
 	AC_MSG_CHECKING(for dracut directory)
 	AC_ARG_WITH([dracutdir],
-		AC_HELP_STRING([--with-dracutdir=DIR],
+		AS_HELP_STRING([--with-dracutdir=DIR],
 		[install dracut helpers @<:@default=check@:>@]),
 		[dracutdir=$withval],
 		[dracutdir=check])
diff --git a/config/user-libexec.m4 b/config/user-libexec.m4
index 31bcea3fcfd..5379c25b4a0 100644
--- a/config/user-libexec.m4
+++ b/config/user-libexec.m4
@@ -1,6 +1,6 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_ZFSEXEC], [
 	AC_ARG_WITH(zfsexecdir,
-		AC_HELP_STRING([--with-zfsexecdir=DIR],
+		AS_HELP_STRING([--with-zfsexecdir=DIR],
 		[install scripts [[@<:@libexecdir@:>@/zfs]]]),
 		[zfsexecdir=$withval],
 		[zfsexecdir="${libexecdir}/zfs"])
diff --git a/config/user-makedev.m4 b/config/user-makedev.m4
index 4383681a8f4..8986107aef8 100644
--- a/config/user-makedev.m4
+++ b/config/user-makedev.m4
@@ -3,13 +3,12 @@ dnl # glibc 2.25
 dnl #
 AC_DEFUN([ZFS_AC_CONFIG_USER_MAKEDEV_IN_SYSMACROS], [
 	AC_MSG_CHECKING([makedev() is declared in sys/sysmacros.h])
-	AC_TRY_COMPILE(
-	[
+	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
 		#include <sys/sysmacros.h>
-	],[
+	]], [[
 		int k;
 		k = makedev(0,0);
-	],[
+	]])],[
 		AC_MSG_RESULT(yes)
 		AC_DEFINE(HAVE_MAKEDEV_IN_SYSMACROS, 1,
 		    [makedev() is declared in sys/sysmacros.h])
@@ -23,13 +22,12 @@ dnl # glibc X < Y < 2.25
 dnl #
 AC_DEFUN([ZFS_AC_CONFIG_USER_MAKEDEV_IN_MKDEV], [
 	AC_MSG_CHECKING([makedev() is declared in sys/mkdev.h])
-	AC_TRY_COMPILE(
-	[
+	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
 		#include <sys/mkdev.h>
-	],[
+	]], [[
 		int k;
 		k = makedev(0,0);
-	],[
+	]])],[
 		AC_MSG_RESULT(yes)
 		AC_DEFINE(HAVE_MAKEDEV_IN_MKDEV, 1,
 		    [makedev() is declared in sys/mkdev.h])
diff --git a/config/user-systemd.m4 b/config/user-systemd.m4
index 3e6a4a281f3..63f02ad2a82 100644
--- a/config/user-systemd.m4
+++ b/config/user-systemd.m4
@@ -1,27 +1,27 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_SYSTEMD], [
 	AC_ARG_ENABLE(systemd,
-		AC_HELP_STRING([--enable-systemd],
+		AS_HELP_STRING([--enable-systemd],
 		[install systemd unit/preset files [[default: yes]]]),
 		[enable_systemd=$enableval],
 		[enable_systemd=check])
 
 	AC_ARG_WITH(systemdunitdir,
-		AC_HELP_STRING([--with-systemdunitdir=DIR],
+		AS_HELP_STRING([--with-systemdunitdir=DIR],
 		[install systemd unit files in dir [[/usr/lib/systemd/system]]]),
 		systemdunitdir=$withval,systemdunitdir=/usr/lib/systemd/system)
 
 	AC_ARG_WITH(systemdpresetdir,
-		AC_HELP_STRING([--with-systemdpresetdir=DIR],
+		AS_HELP_STRING([--with-systemdpresetdir=DIR],
 		[install systemd preset files in dir [[/usr/lib/systemd/system-preset]]]),
 		systemdpresetdir=$withval,systemdpresetdir=/usr/lib/systemd/system-preset)
 
 	AC_ARG_WITH(systemdmodulesloaddir,
-		AC_HELP_STRING([--with-systemdmodulesloaddir=DIR],
+		AS_HELP_STRING([--with-systemdmodulesloaddir=DIR],
 		[install systemd module load files into dir [[/usr/lib/modules-load.d]]]),
 		systemdmodulesloaddir=$withval,systemdmodulesloaddir=/usr/lib/modules-load.d)
 
 	AC_ARG_WITH(systemdgeneratordir,
-		AC_HELP_STRING([--with-systemdgeneratordir=DIR],
+		AS_HELP_STRING([--with-systemdgeneratordir=DIR],
 		[install systemd generators in dir [[/usr/lib/systemd/system-generators]]]),
 		systemdgeneratordir=$withval,systemdgeneratordir=/usr/lib/systemd/system-generators)
 
diff --git a/config/user-sysvinit.m4 b/config/user-sysvinit.m4
index 65dcc381923..b6b63f1cfa3 100644
--- a/config/user-sysvinit.m4
+++ b/config/user-sysvinit.m4
@@ -1,6 +1,6 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_SYSVINIT], [
 	AC_ARG_ENABLE(sysvinit,
-		AC_HELP_STRING([--enable-sysvinit],
+		AS_HELP_STRING([--enable-sysvinit],
 		[install SysV init scripts [default: yes]]),
 		[],enable_sysvinit=yes)
 
diff --git a/config/user-udev.m4 b/config/user-udev.m4
index 65dc79fb484..e6120fc8fef 100644
--- a/config/user-udev.m4
+++ b/config/user-udev.m4
@@ -1,7 +1,7 @@
 AC_DEFUN([ZFS_AC_CONFIG_USER_UDEV], [
 	AC_MSG_CHECKING(for udev directories)
 	AC_ARG_WITH(udevdir,
-		AC_HELP_STRING([--with-udevdir=DIR],
+		AS_HELP_STRING([--with-udevdir=DIR],
 		[install udev helpers @<:@default=check@:>@]),
 		[udevdir=$withval],
 		[udevdir=check])
@@ -18,7 +18,7 @@ AC_DEFUN([ZFS_AC_CONFIG_USER_UDEV], [
 	])
 
 	AC_ARG_WITH(udevruledir,
-		AC_HELP_STRING([--with-udevruledir=DIR],
+		AS_HELP_STRING([--with-udevruledir=DIR],
 		[install udev rules [[UDEVDIR/rules.d]]]),
 		[udevruledir=$withval],
 		[udevruledir="${udevdir}/rules.d"])
diff --git a/config/zfs-build.m4 b/config/zfs-build.m4
index 7754eda3f6a..f0eb47035d1 100644
--- a/config/zfs-build.m4
+++ b/config/zfs-build.m4
@@ -180,7 +180,7 @@ AC_DEFUN([ZFS_AC_CONFIG], [
 		[Config file 'kernel|user|all|srpm']),
 		[ZFS_CONFIG="$withval"])
 	AC_ARG_ENABLE([linux-builtin],
-		[AC_HELP_STRING([--enable-linux-builtin],
+		[AS_HELP_STRING([--enable-linux-builtin],
 		[Configure for builtin in-tree kernel modules @<:@default=no@:>@])],
 		[],
 		[enable_linux_builtin=no])
diff --git a/configure.ac b/configure.ac
index 9ba122e58ff..4520a290a9a 100644
--- a/configure.ac
+++ b/configure.ac
@@ -36,7 +36,7 @@ AC_LANG(C)
 ZFS_AC_META
 AC_CONFIG_AUX_DIR([config])
 AC_CONFIG_MACRO_DIR([config])
-AC_CANONICAL_SYSTEM
+AC_CANONICAL_TARGET
 AM_MAINTAINER_MODE
 m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])
 AM_INIT_AUTOMAKE([subdir-objects])
@@ -45,9 +45,9 @@ AC_CONFIG_HEADERS([zfs_config.h], [
 	awk -f ${ac_srcdir}/config/config.awk zfs_config.h.tmp >zfs_config.h &&
 	rm zfs_config.h.tmp) || exit 1])
 
+LT_INIT
 AC_PROG_INSTALL
 AC_PROG_CC
-AC_PROG_LIBTOOL
 PKG_PROG_PKG_CONFIG
 AM_PROG_AS
 AM_PROG_CC_C_O
