# Maintainer: Jan Houben <jan@nexttrex.de>
# Contributor: Jesus Alvarez <jeezusjr at gmail dot com>
# Maintainer: Víctor González <mrvikxd@gmail.com>

_kernel_suffix="d255"
_kernel="linux-${_kernel_suffix}"
pkgbase="zfs-$_kernel"
pkgname=("$pkgbase" "${pkgbase}-headers")
_zfsver="2.0.0"
_kernelver="5.10.4.arch2-1"
_extramodules="${_kernelver/.arch/-arch}-${_kernel_suffix}"
pkgver="${_zfsver}_$(echo ${_kernelver} | sed s/-/./g)"
pkgrel=1
depends=("${_kernel}")
makedepends=("${_kernel}-headers=${_kernelver}")
arch=("x86_64")
url="https://zfsonlinux.org/"
_patches=(
    autoconf-270-compatibility.patch
)
source=(
    "https://github.com/zfsonlinux/zfs/releases/download/zfs-${_zfsver}/zfs-${_zfsver}.tar.gz"
    "sign_module.sh"
    ${_patches[@]}
)
sha256sums=('3403bf8e993f3c9d772f768142117df47bdbbb8e9bbf85a29c0e166f577f9311'
            'de7154d945e23483fb875aa3ae6807e7565757a34157f109e4dc5aff92284560'
            'dc82ee4e62f76b68d972423909c38ced28dea876c6ef4f19037a24a8dbb2fff5')
license=("CDDL")
depends=("kmod" "zfs-utils=${_zfsver}" "${_kernel}=${_kernelver}")

prepare(){
    cd "${srcdir}/zfs-${_zfsver}"
    for pa in ${_patches[@]}; do
        echo "Patching $pa"
        patch -p1 -i "${srcdir}/$pa"
    done
}

build() {
    cd "${srcdir}/zfs-${_zfsver}"
    ./autogen.sh || true
    ./configure --prefix=/usr --sysconfdir=/etc --sbindir=/usr/bin --libdir=/usr/lib \
                --datadir=/usr/share --includedir=/usr/include --with-udevdir=/lib/udev \
                --libexecdir=/usr/lib/zfs-${_zfsver} --with-config=kernel \
                --with-linux=/usr/lib/modules/${_extramodules}/build \
                --with-linux-obj=/usr/lib/modules/${_extramodules}/build
    make
}

package_zfs-linux-d255() {
    pkgdesc="Kernel modules for the Zettabyte File System."
    install=zfs.install
    provides=("zfs" "spl")
    groups=("archzfs-${_kernel}")
    conflicts=("zfs-dkms" "zfs-dkms-git" "zfs-dkms-rc" "spl-dkms" "spl-dkms-git" "zfs-${_kernel}-git" "zfs-${_kernel}-rc" "spl-${_kernel}")
    replaces=("spl-${_kernel}")
    cd "${srcdir}/zfs-${_zfsver}"
    make DESTDIR="${pkgdir}" install
    cp -r "${pkgdir}"/{lib,usr}
    rm -r "${pkgdir}"/lib
    # Remove src dir
    rm -r "${pkgdir}"/usr/src
    _headers="/usr/src/${_kernel}"
    if [ -n "$SIGNING_KEY_X509" ]; then
        set -e
        find "${pkgdir}/usr/lib/modules/${_extramodules}/extra" -type f -name "*.ko.xz" -exec ${srcdir}/sign_module.sh ${_headers} \{\} \;
    else
        echo "No signing key. Module sign skipped"
    fi
}

package_zfs-linux-d255-headers() {
    pkgdesc="Kernel headers for the Zettabyte File System."
    provides=("zfs-headers" "spl-headers")
    conflicts=("zfs-headers" "zfs-dkms" "zfs-dkms-git" "zfs-dkms-rc" "spl-dkms" "spl-dkms-git" "spl-headers")
    cd "${srcdir}/zfs-${_zfsver}"
    make DESTDIR="${pkgdir}" install
    rm -r "${pkgdir}/lib"
    # Remove reference to ${srcdir}
    sed -i "s+${srcdir}++" ${pkgdir}/usr/src/zfs-*/${_extramodules}/Module.symvers
}
