#!/bin/bash

expected_ver=$(grep _zfsver= PKGBUILD|sed -E 's/_zfsver=//')

echo "Setting version to $expected_ver"
cd zfs-utils
sed -i -E "s/(pkgver=).*/\1$expected_ver/" PKGBUILD
updpkgsums
