#!/bin/bash
set -e

K_HEADERS="$1"
MODULE_PATH="$2"

MOD_NAME=$(basename "$MODULE_PATH" .ko)
CERT=signing_key.x509
KEY="$SIGNING_KEY_X509" #Key is before cert in PEM file

cd /tmp
"$K_HEADERS/scripts/extract-cert" "$SIGNING_KEY_X509" "$CERT"
xz -dk "$MODULE_PATH" --stdout> "$MOD_NAME"
"$K_HEADERS/scripts/sign-file" sha512 "$KEY" "$CERT" $MOD_NAME
xz -z "$MOD_NAME" --stdout> "$MODULE_PATH"
rm -vf "$MOD_NAME"
echo "Signed ${MOD_NAME}"

